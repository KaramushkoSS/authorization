<?php
// bootstrap.php

namespace Doctrine\ORM\EntityManagerInclude;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

class EntityManagerGetter{
    static function get(){
        $paths = array("Entity");
        $isDevMode = false;

        // the connection configuration
        $dbParams = include 'config/db_conf.php';

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        return EntityManager::create($dbParams, $config);
    }
}

