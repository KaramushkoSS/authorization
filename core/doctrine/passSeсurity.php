<?php


namespace Doctrine\ORM\Security;


class passSecurity
{
    public function saltGenerate($saltLength = 8)
    {
        $salt = '';
        for($i=0; $i<$saltLength; $i++) {
            $salt .= chr(mt_rand(33,126)); //символ из ASCII-table
        }
        return $salt;
    }

}