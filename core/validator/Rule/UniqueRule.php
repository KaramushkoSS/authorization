<?php
namespace Rakit\Validation\Rules;

use Doctrine\ORM\EntityManagerInclude\EntityManagerGetter;
use Rakit\Validation\Rule;


class UniqueRule extends Rule
{
    protected $message = ":attribute :value has been used";

    private $em;

    public function __construct()
    {
        $this->em = EntityManagerGetter::get();
    }

    public function check($value): bool
    {
        $db_val = $this->em->getRepository($this->params['entity'])->findOneBy([$this->params['column'] => $value]);
        if ($db_val) {
            return false;
        }

        return true;
    }

    public function entity($entity)
    {
        $this->params['entity'] = $entity;
        return $this;
    }

    public function column($column)
    {
        $this->params['column'] = $column;
        return $this;
    }
}