<?php

namespace App\Core;

class Router
{
    public $routes = [];
    public $requestedUrl = '';
    public $defaultMethod = '';
    public $pageNotFoundController = '';
    public $pageNotFoundMethod = '';
    private $controller = '';
    private $method = '';
    private $params = [];

    public function __construct(array $routes, $defaultMethod = null)
    {
        $this->routes = $routes;
        if ($defaultMethod) {
            $this->defaultMethod = $defaultMethod;
        } else {
            $this->defaultMethod = 'init';
        }

        try {
            $this->checkCorrectRoutes();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function init()
    {
        $this->getPageNotFoundData();

        $this->getRedirectParams();

        $this->runRedirect();
    }

    private function getRedirectParams()
    {
        $this->requestedUrl = explode('?', $_SERVER["REQUEST_URI"])[0];
        if (isset(explode('?', $_SERVER["REQUEST_URI"])[1])) {
            $this->params = explode('&', explode('?', $_SERVER["REQUEST_URI"])[1]);
            foreach ($this->params as $key=>$val){
                $this->params[$key] = [explode('=', $val)[0] => explode('=', $val)[1]];
            }
        } else {
            $this->params = [];
        }
        $defaultPageFlag = true;
        foreach ($this->routes as $route) {
            if ($route['path'] == $this->requestedUrl) {
                $defaultPageFlag = false;

                $this->controller = $route['controller'];
                if (isset($route['method'])) {
                    $this->method = $route['method'];
                } else {
                    $this->method = $this->defaultMethod;
                }
            }
        }

        if ($defaultPageFlag) {
            $this->controller = $this->pageNotFoundController;
            $this->method = $this->pageNotFoundMethod;
        }
    }

    private function runRedirect()
    {
        try{
            require "MVC/controller/" . $this->controller . ".php";
            call_user_func_array([$this->controller, $this->method], $this->params);
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    private function checkCorrectRoutes()
    {
        if (count($this->routes)) {
            foreach ($this->routes as $route) {
                if (!isset($route['path']) || !isset($route['controller'])) {
                    throw new Exception('No correct route mass');
                }
            }
        } else {
            throw new Exception('Route mass is empty');
        }

        return 1;
    }

    private function getPageNotFoundData()
    {
        $pageNotFoundSetFlag = false;

        foreach ($this->routes as $key => $route) {
            if ($route['path'] == '404') {
                $pageNotFoundSetFlag = true;

                $this->pageNotFoundController = $route['controller'];

                if ($route['method']) {
                    $this->pageNotFoundMethod = $route['method'];
                } else {
                    $this->pageNotFoundMethod = $this->defaultMethod;
                }

                unset($this->routes[$key]);
            }
        }

        if (!$pageNotFoundSetFlag) {
            $this->pageNotFoundController = 'defaultController';
            $this->pageNotFoundMethod = 'pageNotFound';
        }
    }
}