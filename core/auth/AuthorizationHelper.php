<?php

namespace App\Core;

use App\Entity\Users;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInclude\EntityManagerGetter;
use Doctrine\ORM\Security\passSecurity;

class AuthorizationHelper
{
    static function login($login, $password, $remember = false)
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        $em = EntityManagerGetter::get();

        $user = $em->getRepository(Users::class)->findOneBy(['login' => $login]);

        if (!$user) {
            $user = $em->getRepository(Users::class)->findOneBy(['mail' => $login]);
        }

        if (!$user) {
            return ['auth' => false, 'cause' => 'Incorrect login'];
        }

        $salt = $user->getSalt();

        if ($user->getPassword() !== md5($password . $salt)) {
            return ['auth' => false, 'cause' => 'Incorrect password'];
        }

        if (!$user->getActive()){
            return ['auth' => false, 'cause' => 'User delete'];
        }

        $_SESSION['auth'] = true;
        $_SESSION['id'] = $user->getId();
        $_SESSION['login'] = $user->getLogin();
        $_SESSION['user_name'] = $user->getFirstName();
        if ($remember) {
            $security = new passSecurity();
            $cookie_auth_key = $security->saltGenerate();
            setcookie('login', $login);
            setcookie('auth_key', $cookie_auth_key);
            $userService = new UserService();
            var_dump($cookie_auth_key);
            $userService->setCookieKey($login, $cookie_auth_key);
        }
        return ['auth' => true];
    }

    static function logout()
    {
        if (isset($_SESSION['auth'])) {
            unset($_SESSION['auth']);
            unset($_SESSION['id']);
            unset($_SESSION['login']);
            unset($_SESSION['user_name']);
            setcookie('login', '', time());
            setcookie('auth_key', '', time());
        }
    }

    static function check_auth()
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        if (!isset($_SESSION['auth']) || !$_SESSION['auth']) {
            if (!empty($_COOKIE['login']) and !empty($_COOKIE['auth_key'])) {
                $em = EntityManagerGetter::get();
                $user = $em->getRepository(Users::class)->findOneBy(['login' => $_COOKIE['login']]);

                if(!$user->getActive()){
                    return false;
                }

                if ($_COOKIE['auth_key'] != $user->getCookieKey() || empty($user->getCookieKey())) {
                    return false;
                } else {
                    $_SESSION['auth'] = true;
                    $_SESSION['id'] = $user->getId();
                    $_SESSION['login'] = $user->getLogin();
                    $_SESSION['user_name'] = $user->getFirstName();
                }
            } else {
                return false;
            }
        } else {
            $em = EntityManagerGetter::get();
            $user = $em->getRepository(Users::class)->find($_SESSION['id']);
            if(!$user->getActive()){
                return false;
            }
        }
        return true;
    }
}