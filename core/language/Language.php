<?php
namespace App\Core;

class Language
{
    protected static $instances;
    private $langs_name = [];
    private $langs = [];
    private $current_lang = '';
    protected function __construct()
    {
        $dir_files = scandir('language');
        foreach ($dir_files as $key => $item){
            if($item != '.' && $item != '..'){
                $this->langs_name[] = str_replace('.json', '', $item);
            }
        }
    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): Language
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

    public function setLang($lang){
        $this->current_lang = $lang;
        if(in_array($this->current_lang, $this->langs_name)){
            $file = file_get_contents('language/'. $this->current_lang .'.json');  // Открыть файл data.json

            $this->langs = json_decode($file,TRUE);        // Декодировать в массив

            unset($file);

            $_SESSION['lang'] = $this->current_lang;
        }
    }

    public function getLang($key){
        return isset($this->langs[$key])? $this->langs[$key] : '';
    }

    public function getCurrentLang(){
        if(isset($_SESSION['lang'])){
            $this->current_lang = $_SESSION['lang'];
        }
        return $this->current_lang;
    }
}