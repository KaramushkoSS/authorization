### Требования к системе ###
###### PHP Version >= 7.1 ######
###### composer ######
###### mySQL Version >= 5.6 ######

### Настройка маршрутизации ###
###### Если в качестве сервера используется nginx нужно настроить переадресацию 404 на index.php, ######
###### Для этого в /etc/nginx/sites-available/example.com измените строку переадресации 404 ошибки на строку "try_files $uri $uri/ /index.html;" ######

### Установка компонентов ###
###### composer install ######

### База данных ###
###### в config/db_conf.php укажите настройки своей бд ######
###### чтобы развернуть БД введите в консоле следующую команду ######
###### ./vendor/bin/doctrine-migrations migrate --configuration config/migration.php ######