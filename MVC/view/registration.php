<?php
require_once "header.php";
?>

<head>
    <link rel="stylesheet" href="/css/reg.css">
    <script src="/js/bootstrap-formhelpers/js/bootstrap-formhelpers-phone.js"></script>
</head>

<section class="section">
    <form name="kek" class="reg-form needs-validation" action="/create-user" enctype="multipart/form-data" method="post"
          novalidate>
        <div class="form-groups">
            <div class="form-group">
                <label for="InputLogin"><?= $lang->getLang('login') ?><span class="danger-text"> *</span></label>
                <input name="login" class="form-control form-control-sm" minlength="3" id="InputLogin"
                       value="<?= isset($_POST['login']) ? $_POST['login'] : '' ?>" required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
            <div class="form-group">
                <label for="InputEmail1"><?= $lang->getLang('mail') ?><span class="danger-text"> *</span></label>
                <input name="mail" type="email" class="form-control form-control-sm" id="InputEmail1"
                       aria-describedby="emailHelp" value="<?= isset($_POST['mail']) ? $_POST['mail'] : '' ?>"
                       required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
            <div class="form-group">
                <label for="InputPassword"><?= $lang->getLang('password') ?><span class="danger-text"> *</span></label>
                <input name="password" type="password" class="form-control form-control-sm"
                       minlength="5" id="InputPassword" required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
        </div>

        <h2 class="center"><?= $lang->getLang('reg_user_info') ?></h2>

        <div class="custom-label"><?= $lang->getLang('avatar') ?><span
                    class="danger-text"> *</span></div>
        <div class="input-group">
            <div class="custom-file">
                <input type="file" name="avatar" accept="image/gif, image/png, image/jpeg"
                       class="custom-file-input form-control-sm" id="inputGroupFile04"
                       onchange="set_name_file_in_lable(this.files)" aria-describedby="inputGroupFileAddon04" required
                >
                <label class="custom-file-label"
                       for="inputGroupFile04"><?= !empty($_POST['fileName']) ? $_POST['fileName'] : $lang->getLang('change_file') ?>
                </label>
            </div>
            <input name="fileName" class="d-none" id="fileName"
                   value="<?= isset($_POST['fileName']) ? $_POST['fileName'] : '' ?>">
            <input name="filePath" class="d-none" id="filePath"
                   value="<?= isset($_POST['filePath']) ? $_POST['filePath'] : '' ?>">
        </div>
        <div class="form-groups mt-4">

            <div class="form-group">
                <label for="InputName"><?= $lang->getLang('firstName') ?><span
                            class="danger-text"> *</span></label>
                <input name="firstName" class="form-control form-control-sm" minlength="3" id="InputName"
                       value="<?= isset($_POST['firstName']) ? $_POST['firstName'] : '' ?>" required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
            <div class="form-group">
                <label for="InputLastName"><?= $lang->getLang('lastName') ?><span
                            class="danger-text"> *</span></label>
                <input name="lastName" class="form-control form-control-sm" id="InputLastName"
                       aria-describedby="emailHelp" value="<?= isset($_POST['lastName']) ? $_POST['lastName'] : '' ?>" required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
            <div class="form-group">
                <label for="InputBirthday"><?= $lang->getLang('birthday') ?><span
                            class="danger-text"> *</span></label>
                <input name="birthday" type="date" class="form-control form-control-sm" minlength="5" id="InputBirthday"
                       value="<?= isset($_POST['birthday']) ? $_POST['birthday'] : '' ?>"
                       required>
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>

            </div>

            <div class="form-group">
                <label for="InputPhone"><?= $lang->getLang('phone') ?></label>
                <input name="phone" type="text" class="form-control form-control-sm input-medium bfh-phone"
                       data-format="+7 (ddd) ddd-dd-dd" id="InputPhone"
                       value="<?= isset($_POST['phone']) ? $_POST['phone'] : '' ?>">
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>
            <div class="form-group">
                <label for="InputAddress"><?= $lang->getLang('address') ?></label>
                <input name="address" type="text" class="form-control form-control-sm" minlength="5" id="InputAddress"
                       value="<?= isset($_POST['address']) ? $_POST['address'] : '' ?>">
                <div class="invalid-feedback"><?= $lang->getLang('reg_validation') ?></div>
            </div>

        </div>
        <div class="form-group form-check">
            <input name="remember_me" type="checkbox" class="form-check-input"
                   id="remember_me" <?= isset($_POST['remember_me']) ? "checked" : '' ?>>
            <label class="form-check-label" for="remember_me"><?= $lang->getLang('remember_me') ?></label>
        </div>
        <!--        <div class="form-group form-check">-->
        <!--            <input name="remember_me" type="checkbox" class="form-check-input" id="exampleCheck1"-->
        <!--                   value="--><? //= isset($_POST['remember_me']) ? $_POST['remember_me'] : '' ?><!--">-->
        <!--            <label for="exampleCheck1">--><? //= $lang->getLang('remember_me') ?><!--</label>-->
        <!--        </div>-->
        <?php
        if (!empty($errors)) {
            $error_block = '<div class="danger-text"><h5 class="bold">' . $lang->getLang('pre_validation_mess') . '</h5>';
            foreach ($errors->firstOfAll() as $field_name => $err) {
                //this field and field fileName indicates the same error
                if ($field_name != 'filePath') {
                    $error_block .= '<p>' . $lang->getLang('field') . ' ' . $lang->getLang($field_name) . ': ' . $err . '</p>';
                }
            };
            $error_block .= '</div>';
            echo $error_block;
        }
        ?>
        <button type="submit" class="btn btn-primary"><?= $lang->getLang('registration_btn') ?></button>
    </form>
</section>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    let set_name_file_in_lable = function (files) {
        let file = files[0];
        $('[for="inputGroupFile04"]').html(file.name);
    }

</script>