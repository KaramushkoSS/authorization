<?php
require_once "header.php";

?>
<head>
    <link rel="stylesheet" href="/css/auth.css">
</head>

<div class="auth-block">
    <form action="login" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1"><?=$lang->getLang('auth_login')?></label>
            <input name="login" class="form-control" id="exampleInputEmail1" value="<?= isset($login['login_val'])? $login['login_val'] : ''?>" aria-describedby="emailHelp">
            <?= (isset($auth['login']) && $auth['login'] == 0) ?
                '<div class="err-mess">' . $lang->getLang('err_login') . '</div>' :
                ''
            ?>
            <?= (isset($auth['user_del'])) ?
                '<div class="err-mess">' . $lang->getLang('user_delete') . '</div>' :
                ''
            ?>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1"><?=$lang->getLang('password')?></label>
            <input name="password" type="password" class="form-control" id="exampleInputPassword1">
            <?= (isset($auth['pass']) && $auth['pass'] == 0) ?
                '<div class="err-mess">' . $lang->getLang('err_pass') . '</div>' :
                ''
            ?>
        </div>
        <div class="form-group form-check">
            <input name="remember_me" type="checkbox" class="form-check-input" id="remember_me">
            <label class="form-check-label" for="remember_me"><?=$lang->getLang('remember_me')?></label>
        </div>

        <button type="submit" class="btn btn-primary btn-block"><?=$lang->getLang('login_btn')?></button>
    </form>
    <a href="registration"><button class="btn btn-success btn-block mt-1"><?=$lang->getLang('registration_btn')?></button></a>
</div>