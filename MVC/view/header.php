<?php
use App\Core\AuthorizationHelper;
use App\Core\Language;

$lang = Language::getInstance();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>TR Logic</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css">

    <link rel="stylesheet" href="/css/main.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="js/setLang.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark ">
    <a class="navbar-brand pl-2 pr-2" href="#">TR Logic</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item ">
                <a class="nav-link" href="/"><?= $lang->getLang('menu_home') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><?= $lang->getLang('menu_about_us') ?></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="#"><?= $lang->getLang('menu_contacts') ?></a>
            </li>

        </ul>
        <div class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <?= $lang->getLang('menu_change_lang') ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" onclick="setLang('ru')">Русский</a>
                        <a class="dropdown-item" onclick="setLang('en')">English</a>
                    </div>
                </li>
            </ul>
            <?php
                if (AuthorizationHelper::check_auth()):
            ?>
                    <ul class="navbar-nav mr-4">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <?= $_SESSION['user_name'] ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="profile"><?= $lang->getLang('profile') ?></a>
                                <a class="dropdown-item" href="logout"><?= $lang->getLang('logout') ?></a>
                            </div>
                        </li>
                    </ul>
            <?php
                else:
            ?>
            <a href="/authorization">
                <button class="btn btn-outline-primary my-2 my-sm-0"
                ><?= $lang->getLang('menu_auth') ?></button>
            </a>
            <?php
                endif;
            ?>
        </div>
    </div>
</nav>



