<?php
require_once "header.php";

?>
<head>
    <link rel="stylesheet" href="/css/profile.css">
</head>

<section class="section">
    <div class="row">
        <div class="avatar">
            <img src="<?=$user_data['avatar']; ?>">
        </div>
        <div class="user-info">
            <h4 class="right"><?= $user_data['last_name'] . ' ' . $user_data['name']?></h4>

            <p>Email: <?=$user_data['email']?></p>

            <p><?=$lang->getLang('birthday') . ': ' . $user_data['birthday']?></p>

            <?php if (isset($user_data['phone']) ):?>
            <p><?=$lang->getLang('phone') . ': ' . $user_data['phone']?></p>
            <?php endif;?>


            <?php if (isset($user_data['address']) ):?>
            <p><?=$lang->getLang('address') . ': ' . $user_data['address']?></p>
            <?php endif;?>
        </div>
    </div>
</section>