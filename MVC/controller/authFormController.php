<?php


use App\Core\AuthorizationHelper;

class authFormController
{
    public function init($login = null, $auth = null)
    {
        if (AuthorizationHelper::check_auth()) {
            header('Location: /profile');
        }
        require_once 'MVC/view/auth.php';
    }

    public function login()
    {
        $login = isset($_POST['login']) ? $_POST['login'] : '';
        $pass = isset($_POST['login']) ? $_POST['password'] : '';
        $remember = isset($_POST['remember_me']) ? $_POST['remember_me'] : false;
        $result = AuthorizationHelper::login($login, $pass,$remember);
        if ($result['auth']) {
            header('Location: /profile');
        } elseif ($result['cause'] == "Incorrect login") {
            header('Location: authorization?login_val=' . $login . '&login=0');
        } elseif ($result['cause'] == "Incorrect password") {
            header('Location: authorization?login_val=' . $login . '&pass=0');
        } elseif ($result['cause'] == "User delete") {
            header('Location: authorization?login_val=' . $login . '&user_del=true');
        }
    }

    public function logout()
    {
        AuthorizationHelper::logout();
        header('Location: /');
    }

    public function userDelete(){
        require_once "MVC/view/userDelete.php";
    }
}