<?php

use App\Core\AuthorizationHelper;
use App\Service\UserService;

class profileController
{
    public function init()
    {
        $user_service = new UserService();
        if(!isset($_SESSION['id'])){
            if(!AuthorizationHelper::check_auth()){
                header('Location: /authorization');
            }
        }
        $user_data = $user_service->getUser($_SESSION['id']);

        require_once 'MVC/view/profile.php';
    }
}