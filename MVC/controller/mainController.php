<?php

class mainController
{
    public function init()
    {

        require_once 'MVC/view/main.php';
    }

    public function pageNotFound()
    {
        require_once "MVC/view/404.php";
    }

    public function setLang()
    {
        $_SESSION['lang'] = $_POST['lang'];
    }
}