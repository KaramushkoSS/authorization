<?php

use App\Core\AuthorizationHelper;
use App\Core\Language;
use App\Entity\Users;
use App\Service\UserService;
use Rakit\Validation\Rules\UniqueRule;
use Rakit\Validation\Validator;

class registrationFormController
{
    public function init()
    {
        require_once 'MVC/view/registration.php';
    }

    public function createUser()
    {
        $lang = Language::getInstance();

        $params = $_POST;

        //set custom err message
        $validator = new Validator([
            'required' => $lang->getLang('reg_validation'),
            'email' => $lang->getLang('email_validation'),
            'uploaded_file:0,500K' => $lang->getLang('file_size_validation'),
            'uploaded_file:png,jpeg,gif' => $lang->getLang('file_type_validation')
        ]);

        //add custom validate rule
        $validator->addValidator('unique', new UniqueRule());

        //set field=>rules for validate
        $validation = $validator->make($_POST + $_FILES, [
            'login' => ['required', 'min:3', $validator('unique')->entity(Users::class)->column('login')->message($lang->getLang('unique_validation'))],
            'password' => 'required|min:6',
            'mail' => ['required', 'email', $validator('unique')->entity(Users::class)->column('mail')->message($lang->getLang('unique_validation'))],
            'avatar' => !empty($_FILES['avatar']['name']) ? 'required|uploaded_file:0,500K,png,jpeg,gif' : '',
            'fileName' => !empty($_FILES['avatar']['name']) ? '' : 'required',
            'filePath' => !empty($_FILES['avatar']['name']) ? '' : 'required',
            'firstName' => 'required|min:2',
            'lastName' => 'required|min:2',
            'birthday' => 'required'
        ]);

        $validation->validate();

        //save new file
        if (!empty($_FILES['avatar']['name'])) {
            $filename = basename($_FILES['avatar']['tmp_name']);
            $file_type = explode('.', $_FILES['avatar']['name']);
            $file_type = '.' . $file_type[count($file_type) - 1];
            $upload_file_path = 'upload/users/img/' . $filename . $file_type;
            if (!mkdir('upload/users/img', 0755, true)){
                throw new Exception('Failed to create a directory');
            }
            move_uploaded_file($_FILES['avatar']['tmp_name'], $upload_file_path);
            $params['avatar'] = $filename . $file_type;
            $_POST['fileName'] = $_FILES['avatar']['name'];
            $_POST['filePath'] = $_FILES['avatar']['tmp_name'];
        } elseif (!empty($_POST['fileName']) && !empty($_POST['filePath'])) {
            $filename = basename($_POST['filePath']);
            $file_type = explode('.', $_POST['fileName']);
            $file_type = '.' . $file_type[count($file_type) - 1];
            $params['avatar'] = $filename . $file_type;
        }

        if ($validation->fails()) {
            $errors = $validation->errors();
            require_once 'MVC/view/registration.php';
        } else {
            $userService = new UserService();
            $userService->addUser($params);

            AuthorizationHelper::login($params['login'], $params['password'], $params['remember_me']);
            header('Location: /profile');
        }
    }
}