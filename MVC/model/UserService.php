<?php

namespace App\Service;

use App\Core\Language;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInclude\EntityManagerGetter;
use Doctrine\ORM\ORMException;

class UserService
{
    private $em;
    private $user;

    public function __construct()
    {
        $this->em = EntityManagerGetter::get();
        $this->user = new Users();
    }

    public function addUser(?array $params)
    {
        try {
            $this->user->createUser($params);
            $this->em->persist($this->user);
            $this->em->flush();
        } catch (\Doctrine\ORM\ORMException $e) {
            echo $e->getMessage();
        }
    }

    public function setCookieKey(?string $login, ?string $cookie_auth_key)
    {
        try {
            $user = $this->em->getRepository(Users::class)->findOneBy(['login' => $login]);
            if (!$user) {
                $user = $this->em->getRepository(Users::class)->findOneBy(['mail' => $login]);
            }

            $user->setCookieKey($cookie_auth_key);
            $this->em->flush();
        } catch (ORMException $e) {
            echo $e->getMessage();
        }
    }

    public function getUser(?string $find_str, ?string $field = null):array
    {
        if ($field) {
            $user = $this->em->getRepository(Users::class)->findOneBy([$field => $find_str]);
        } else {
            $user = $this->em->getRepository(Users::class)->find($find_str);
        }

        $data = [];
        $data['name'] = $user->getFirstName();
        $data['last_name'] = $user->getLastName();
        $data['avatar'] = 'upload/users/img/' . $user->getAvatar();
        $data['email'] = $user->getMail();

        $data['birthday'] = $user->getBirthday()->format('j n Y');
        $month = explode(' ', $data['birthday'])[1];
        $lang = Language::getInstance();
        $month = $lang->getLang('birthday_month')[$month];
        $data['birthday'] = explode(' ', $data['birthday'])[0] . ' ' . $month . ' ' . explode(' ', $data['birthday'])[2];
        unset($month);

        $data['create'] = $user->getCreatedAt()->format('Y-m-d');

        if (!empty($user->getPhone())) {
            $data['phone'] = $user->getPhone();
        }

        if (!empty($user->getAddress())) {
            $data['address'] = $user->getAddress();
        }

        $data['active'] = $user->getActive();

        return $data;
    }
}