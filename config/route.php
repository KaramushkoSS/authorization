<?php
//for add route required set "path" and "controller"
//available to set value "method", their default value "init"
//available to set "404" page redirect
//default 404 method: defaultController->pageNotFound

$route = [
    [
        "path" => "/",
        "controller" => "mainController"
    ],
    [
        "path" => "/authorization",
        "controller" => "authFormController",
    ],
    [
        "path" => "/login",
        "controller" => "authFormController",
        "method" => "login"
    ],
    [
        "path" => "/logout",
        "controller" => "authFormController",
        "method" => "logout"
    ],
    [
        "path" => "/registration",
        "controller" => "registrationFormController"
    ],
    [
        "path" => "/profile",
        "controller" => "profileController"
    ],
    [
        "path" => "/set-lang",
        "controller" => "mainController",
        "method" => "setLang"
    ],
    [
        "path" => "/create-user",
        "controller" => "registrationFormController",
        "method" => "createUser"
    ],
    [
        'path' => "/user-delete",
        "controller" => "authFormController",
        "method" => "userDelete"
    ],
    [
        "path" => "404",
        "controller" => "mainController",
        "method" => "pageNotFound"
    ]
];

$defaultMethod = "init";