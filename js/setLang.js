function setLang(lang) {
    $.ajax({
        url: 'set-lang',
        type: "POST",
        data: {'lang': lang},
        success: function (res) {
            location.reload()
        }
    })
}