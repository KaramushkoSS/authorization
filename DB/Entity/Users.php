<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;
use DateTime;
use Doctrine\ORM\Security\passSecurity;

/** @Entity
 * @Table(name="Users",indexes={@Index(name="search_idx", columns={"login"})})
 */
class Users
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="string", unique=true) */
    private $login;

    /** @Column(type="string") */
    private $password;

    /** @Column(type="string") */
    private $salt;

    /** @Column(type="string", name="cookie_key", nullable=true) */
    private $cookieKey;

    /** @Column(type="string") */
    private $avatar;

    /** @Column(type="string", name="first_name") */
    private $firstName;

    /** @Column(type="string", name="last_name") */
    private $lastName;

    /** @Column(type="datetime", nullable=true) */
    private $birthday;

    /** @Column(type="string", nullable=true) */
    private $phone;

    /** @Column(type="string", nullable=true) */
    private $address;

    /** @Column(type="string", unique=true) */
    private $mail;

    /** @Column(type="datetime", name="created_at", nullable=true) */
    private $createdAt;

    /** @Column(type="boolean") */
    private $active = true;

    public function __construct()
    {
        $this->createdAt = new DateTime("now");
    }

    public function createUser(array $params): self
    {
        foreach ($params as $key => $value) {
            if ($key == 'birthday' && $value != '') {
                $this->setBirthday($value);
            } elseif ($key == 'password' && strlen($value) >= 3) {
                $this->setPassword($value);
            } else {
                $this->$key = $value;
            }
        }


        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): self
    {
        $this->login = $login;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $security = new passSecurity();
        $salt = $security->saltGenerate();
        $this->setSalt($salt);
        $this->password = md5($password . $salt);
        return $this;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;
        return $this;
    }

    public function getCookieKey(): string
    {
        return $this->cookieKey;
    }

    public function setCookieKey($key): self
    {
        $this->cookieKey = $key;
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getBirthday(): ?object
    {
        return $this->birthday;
    }

    public function setBirthday(?string $birthday): self
    {
        $birthday = new DateTime($birthday);
        $this->birthday = $birthday;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }


    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;
        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
}