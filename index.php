<?php
//include any php files in path
$include_file_dirs_arr = ['MVC/model', 'DB/Entity', 'core'];

foreach ($include_file_dirs_arr as $include_file_dir_arr){
    require_php_files_in_path($include_file_dir_arr);
}

function require_php_files_in_path($path){
    $dir_files = scandir($path);
    foreach ($dir_files as $key => $item){
        if($item != '.' && $item != '..'){
            if(strripos($item, '.php') === false){
                require_php_files_in_path($path . '/' . $item);
            } else{
                require $path . '/' . $item;
            }
        }
    }
}

session_start();
#=====================================================================
//TO DO create config file with options, for include core tools and create tool for autocompile this
//but now we have only language
use App\Core\Language;

$lang = Language::getInstance();
if (isset($_SESSION['lang'])) {
    $lang->setLang($_SESSION['lang']);
} else {
    $lang->setLang('en');
}


#=====================================================================
require_once "config/route.php";
require_once "vendor/autoload.php";

use App\Core\Router;


//start custom Route
$router = new Router($route);
$router->init();
